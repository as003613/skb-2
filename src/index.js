import express from 'express';
import cors from 'cors';

const app = express();
app.use(cors());
app.get('/', (req, res) => {
  res.json({
    hello: 'JS World',
  });
});

app.get('/task2A', (req, res) => {
  var a = Number(req.query.a) || 0;
  var b = Number(req.query.b) || 0;
  res.send(`${a + b}`);
});

app.get('/task2B', (req, res) => {
  var fullname = req.query.fullname;
  if (!fullname) {
    return res.send('Invalid fullname');
  }
  fullname = fullname.split(' ');
  if (fullname.length > 3 || fullname.length == 0) {
    return res.send('Invalid fullname');
  }

  fullname.forEach(v => {
    if ((/(\d|_|\/)/.test(v)))
      return res.send('Invalid fullname');
  });

  if (fullname.length == 1) {
    return res.send(`${fullname[0]}`);
  }
  if (fullname.length == 2) {
    return res.send(`${fullname[1]} ${fullname[0][0]}.`);
  }

  return res.send(`${fullname[2]} ${fullname[0][0]}. ${fullname[1][0]}.`);

});

app.get('/task2C', (req, res) => {
  var username = req.query.username;
  var clearUsername = username.match(/(^\w+\.?\w+$)|[a-z0-9\-]+\.[a-z0-9\-]+\/(\w+\.?\w+)|@(\w+\.?\w+)/).splice(1).filter(v => v);
  res.send(`@${clearUsername}`);
});

app.get('/task2D', (req, res) => {
  let color = req.query.color;
  let regexp = /^\s*#?([0-9A-Fa-f]{3}|[0-9A-Fa-f]{6})\s*$/;
  console.log(color);
  if (!color || !(regexp.test(color))) {
    return res.send(`Invalid color`);
  }
  console.log(color.length);
  color = color.match(regexp)[1].toLowerCase();
  if (color.length == 3) {
    return res.send('#' + color[0] + color[0] + color[1] + color[1] + color[2] + color[2]);
  }
  return res.send('#' + color);
});

app.listen(3000, () => {
  console.log('Your app listening on port 3000!');
});
